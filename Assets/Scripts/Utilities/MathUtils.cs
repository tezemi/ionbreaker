﻿
namespace Ionbreaker.Utilities
{
    public static class MathUtils
    {
        public static float GetRange(float input, float inputLow, float inputHigh, float outputHigh, float outputLow)
        {
            return (input - inputLow) / (inputHigh - inputLow) * (outputHigh - outputLow) + outputLow;
        }
    }
}
