﻿using UnityEngine;

namespace Ionbreaker.Physics
{
    [DisallowMultipleComponent]
    public class Ionbody : MonoBehaviour
    {
        public float LinearDrag = 1f;
        public Vector2 Velocity { get; set; }

        protected virtual void FixedUpdate()
        {
            
        }
    }
}