﻿using Ionbreaker.Utilities;
using UnityEngine;

namespace Ionbreaker.Physics
{
    public class DynamicGravityEmitter : GravityEmitter
    {        
        public float RadiusScale = 0.025f;
        public float IntensityScale = 5f;
        
        protected override void FixedUpdate()
        {
            base.FixedUpdate();

            if (DisableGravity) return;

            int numberOfObjectsInField = 0;
            foreach (var body in Rigidbodies)
            {
                if (body != null && Vector2.Distance(body.transform.position, transform.position) < Radius)
                {
                    numberOfObjectsInField++;
                }
            }

            Radius = MathUtils.GetRange(numberOfObjectsInField, 0f, 1f, RadiusScale, 0f) + 1f;
            Intensity = MathUtils.GetRange(numberOfObjectsInField, 0f, 1f, IntensityScale, 0f);            
        }
    }
}