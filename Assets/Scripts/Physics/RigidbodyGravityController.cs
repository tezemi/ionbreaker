﻿using UnityEngine;

namespace Ionbreaker.Physics
{
    public class RigidbodyGravityController : MonoBehaviour
    {
        public DynamicGravityEmitter ParentGravityEmitter;

        protected virtual void FixedUpdate()
        {
            float distance = Vector2.Distance(ParentGravityEmitter.transform.position, transform.position);
            if (ParentGravityEmitter.transform.parent != transform && distance > ParentGravityEmitter.Radius / 2f)
            {
                ParentGravityEmitter.transform.SetParent(transform);
            }
        }
    }
}