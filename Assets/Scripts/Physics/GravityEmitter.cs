﻿using Ionbreaker.Utilities;
using UnityEngine;

namespace Ionbreaker.Physics
{
    [DisallowMultipleComponent]
    public class GravityEmitter : MonoBehaviour
    {
        public bool DisableGravity;
        public float Radius = 5f;
        public float Intensity = 1500f;
        protected Rigidbody2D[] Rigidbodies { get; private set; }

        protected virtual void OnEnable()
        {
            Rigidbodies = FindObjectsOfType<Rigidbody2D>();
        }

        protected virtual void FixedUpdate()
        {
            if (DisableGravity) return;

            foreach (var body in Rigidbodies)
            {
                float distance = Vector2.Distance(body.transform.position, transform.position);
                if (distance < Radius)
                {
                    float intensity = MathUtils.GetRange(distance, 0f, Radius, 1f, Intensity);
                    body.AddForce((transform.position - body.transform.position).normalized * intensity * Time.smoothDeltaTime);
                }                
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (enabled && !DisableGravity)
            {
                Gizmos.color = Color.blue;
                Gizmos.DrawWireSphere(transform.position, Radius);
            }
        }
    }
}