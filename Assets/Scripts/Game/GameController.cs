﻿using Ionbreaker.Player;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using UnityEngine;

namespace Ionbreaker.GameState
{
    public class GameController : MonoBehaviour
    {
        public int DebugPlanetX = 16;
        public int DebugPlanetY = 16;
        public GameObject Rock;
        public GameMode DebugGameMode;
        public List<GamePlayer> Players = new List<GamePlayer>();

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.Return))
            {
                StartGame();
            }
        }

        public void StartGame()
        {
            GamePlayer bob = new GamePlayer("Bob");
            PlayerController.Main.GamePlayer = bob;


            var entityManager = World.Active.EntityManager;
            NativeArray<Entity> rocks = new NativeArray<Entity>(DebugPlanetX * DebugPlanetY, Allocator.Persistent);
            entityManager.Instantiate(Rock, rocks);

            int count = 0;
            for (int x = 0; x < DebugPlanetX; x++)
            {
                for (int y = 0; y < DebugPlanetY; y++)
                {
                    entityManager.SetComponentData(rocks[count++], new Translation { Value = new Vector3(x * 0.27f, y * 0.27f, 0f) });
                }
            }

            rocks.Dispose();

            foreach (var construct in DebugGameMode.StartingConstructs)
            {
                PlayerController.Main.GamePlayer.GiveConstruct(construct.gameObject, new Vector3(DebugPlanetX + 2f, DebugPlanetY + 2f, 0f), true);
            }
        }
    }
}
