﻿using Ionbreaker.Constructs;
using UnityEngine;

namespace Ionbreaker.GameState
{
    [CreateAssetMenu(fileName = "New Game Mode", menuName = "Data/Game Mode")]
    public class GameMode : ScriptableObject
    {
        [Tooltip("The maximum number of players allowed to participate in this game.")]
        public int MaxPlayers = 8;
        [Tooltip("The minimum number players needed to be able to play this game.")]
        public int RequiredPlayers = 2;
        [Tooltip("The amount of space dust each player will start with.")]
        public int StartingSpaceDust = 1000;
        [Tooltip("The time the game will last in minutes. Set this to zero to never end on a timer.")]
        public float GameTime = 0f;
        [Tooltip("What constructs the player will start with when the game starts.")]
        public Construct[] StartingConstructs;
    }
}

