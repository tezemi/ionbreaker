﻿using Ionbreaker.Player;
using System;
using UnityEngine;
using UnityEngine.UI;

namespace Ionbreaker.GUI
{
    public class HUDController : MonoBehaviour
    {
        public Text PlayerStatusText;
        public Text ConstructStatusText;
        public Text HotkeyInfo;

        protected virtual void Update()
        {
            if (PlayerController.Main.GamePlayer == null) return;

            PlayerStatusText.text = $"Current Player: {PlayerController.Main.GamePlayer.Name}{Environment.NewLine}Space Dust: {PlayerController.Main.GamePlayer.SpaceDust}";
            ConstructStatusText.text = $"{PlayerController.Main.Focus.Name}{Environment.NewLine}{PlayerController.Main.Focus.Health} / {PlayerController.Main.Focus.MaxHealth}";

            var focusedConstruct = PlayerController.Main.Focus;
            var hotkeyInfoString = string.Empty;
            foreach (var hotkeyInfo in focusedConstruct.HotkeyInfo)
            {
                hotkeyInfoString += $"{hotkeyInfo.ActivateInput} - {hotkeyInfo.Name}{Environment.NewLine}";
            }

            HotkeyInfo.text = hotkeyInfoString;
        }
    }
}
