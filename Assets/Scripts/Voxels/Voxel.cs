﻿using UnityEngine;

namespace Ionbreaker.Voxels
{
    public class Voxel : MonoBehaviour
    {
        public int Weakness = 5;
        public int Health { get; set; } = 100;
        public const int MinHealth = 0;
        public const int MaxHealth = 100;
        
        public void Mine(float damageModifier)
        {
            Health -= (int)(Weakness * damageModifier);
            UnityEngine.Debug.Log($"Health: {Health}", gameObject);
            if (Health <= 0)
            {
                Destroy(gameObject);
            }
        }
    }
}