﻿using Ionbreaker.Constructs;
using System.Collections.Generic;
using UnityEngine;

namespace Ionbreaker.Player
{
    public class GamePlayer
    {
        public string Name { get; set; }
        public int SpaceDust { get; set; } = 1000;
        public List<Construct> Constructs { get; set; } = new List<Construct>();

        public GamePlayer(string name)
        {
            Name = name;
        }

        public GameObject GiveConstruct(GameObject prefab, Vector3 position, bool shouldFocus)
        {
            var obj = Object.Instantiate(prefab);
            obj.transform.position = position;
            obj.GetComponent<Construct>().Owner = this;

            if (shouldFocus)
            {
                PlayerController.Main.Focus = obj.GetComponent<Construct>();
            }

            Constructs.Add(obj.GetComponent<Construct>());

            return obj;
        }
    }
}
