﻿using UnityEngine;

namespace Ionbreaker.Player
{
    [DisallowMultipleComponent]
    public class CameraController : MonoBehaviour
    {
        public static CameraController Main { get; protected set; }
        public GameObject Focus;
        
        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void Update()
        {
            if (Focus != null)
            {
                transform.position = new Vector3(Focus.transform.position.x, Focus.transform.position.y, transform.position.z);
            }
        }
    }
}