﻿using Ionbreaker.Constructs;
using UnityEngine;

namespace Ionbreaker.Player
{
    public class PlayerController : MonoBehaviour
    {
        public static PlayerController Main { get; protected set;  }
        
        /// <summary>
        /// The player that is currently playing the game, this is usually the 
        /// local player running on this client.
        /// </summary>
        public GamePlayer GamePlayer { get; set; }
        private Construct _focus;

        public Construct Focus
        {
            get
            {
                return _focus;
            }
            set
            {
                if (_focus != null)
                {
                    _focus.Focused = false;
                }                
                
                _focus = value;
                _focus.Focused = true;
                CameraController.Main.Focus = value.gameObject;
            }
        }

        protected virtual void Awake()
        {
            if (Main == null)
            {
                Main = this;
            }
        }

        protected virtual void Update()
        {
            if (Input.GetKeyDown(KeyCode.RightShift))
            {
                var focusableObjects = GamePlayer.Constructs;
                int focusIndex = 0;
                for (int i = 0; i < focusableObjects.Count; i++)
                {
                    if (focusableObjects[i] == _focus && i < focusableObjects.Count - 1)
                    {
                        focusIndex = i + 1;
                        break;
                    }
                }

                Focus = focusableObjects[focusIndex];
            }
            else if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                var focusableObjects = GamePlayer.Constructs;
                int focusIndex = 0;
                for (int i = focusableObjects.Count - 1; i > 0; i--)
                {
                    if (focusableObjects[i] == _focus && i >= 1)
                    {
                        focusIndex = i - 1;
                        break;
                    }
                }

                Focus = focusableObjects[focusIndex];
            }
        }
    }
}
