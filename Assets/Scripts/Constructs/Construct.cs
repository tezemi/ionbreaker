﻿using System.Collections.Generic;
using Ionbreaker.Player;
using Ionbreaker.Utilities;
using UnityEngine;

namespace Ionbreaker.Constructs
{
    [DisallowMultipleComponent]
    public class Construct : MonoBehaviour
    {
        public int Health = 100;
        public int MaxHealth = 100;
        public string Name = "Name Not Set";
        public List<HotkeyInfo> HotkeyInfo = new List<HotkeyInfo>();
        public bool Focused { get; set; }
        public GamePlayer Owner { get; set; }
        public Queue<ConstructionJob> ConstructionJobQueue { get; protected set; } = new Queue<ConstructionJob>();

        protected virtual void OnEnable()
        {
            Timing.RunCoroutine(DoConstructionJobQueue(), Segment.FixedUpdate);
        }

        protected IEnumerator<float> DoConstructionJobQueue()
        {
            while (isActiveAndEnabled)
            {
                yield return Timing.WaitUntilDone(() => ConstructionJobQueue.Count > 0);
                yield return Timing.WaitForSeconds(ConstructionJobQueue.Peek().Time);
                Owner.GiveConstruct(ConstructionJobQueue.Peek().Prefab, transform.position, false);
                ConstructionJobQueue.Dequeue();
                yield return Timing.WaitForOneFrame;
            }
        }

        public virtual void Hurt(int damage)
        {
            Health -= damage;
            if (Health <= 0)
            {
                Health = 0;
                Die();
            }
        }

        public virtual void Die()
        {
            Destroy(gameObject);
        }

        public void BeginConstruction(ConstructionJob job)
        {
            Owner.SpaceDust -= job.Cost;
            ConstructionJobQueue.Enqueue(job);
        }
    }
}