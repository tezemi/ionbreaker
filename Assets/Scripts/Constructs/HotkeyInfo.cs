﻿using UnityEngine;

namespace Ionbreaker.Constructs
{
    [CreateAssetMenu(fileName = "New Hotkey Info", menuName = "Data/Hotkey Info")]
    public class HotkeyInfo : ScriptableObject
    {
        public string Name = "Name Not Set";
        public KeyCode ActivateInput;
        public Sprite Icon;
    }
}

