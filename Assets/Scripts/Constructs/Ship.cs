﻿using UnityEngine;

namespace Ionbreaker.Constructs
{
    /// <summary>
    /// Base class for ship constructs that can be flown around by players. 
    /// Handles basic movement.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class Ship : Construct
    {
        public float ThurstSpeed = 30f;
        public float TurningSpeed = 1f;
        public float TractorBeamStrength = 500f;
        public GameObject ProjectilePrefab;
        public float TractoringObjectDrag { get; set; }
        public Rigidbody2D TractoringObject { get; set; }
        public const float TractoredObjectDrag = 5f;
        public const float TractorBeamDistanceToActivate = 2f;
        public const float TractorBeamDistanceBehind = 1.5f;
        protected Rigidbody2D Rigidbody2D { get; set; }
        protected Rigidbody2D[] Rigidbodies { get; set; }

        protected virtual void Awake()
        {
            Rigidbody2D = GetComponent<Rigidbody2D>();
            Rigidbodies = FindObjectsOfType<Rigidbody2D>();
        }

        protected virtual void Update()
        {
            if (!Focused) return;

            // Movement
            if (Input.GetKey(KeyCode.UpArrow))
            {
                Rigidbody2D.AddForce(transform.up * ThurstSpeed);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                Rigidbody2D.AddForce(-transform.up * ThurstSpeed);
            }

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                Rigidbody2D.AddTorque(TurningSpeed);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                Rigidbody2D.AddTorque(-TurningSpeed);
            }

            if (Input.GetKeyDown(KeyCode.Space))
            {
                var projectleGameObject = Instantiate(ProjectilePrefab);
                Physics2D.IgnoreCollision(projectleGameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
                projectleGameObject.transform.position = transform.position;
                projectleGameObject.transform.rotation = transform.rotation;
                projectleGameObject.GetComponent<Projectile>().Fire();
            }

            if (Input.GetKeyDown(KeyCode.Alpha1)) // Tractor Beam
            {
                if (TractoringObject == null)
                {
                    float lastSmallestDistance = TractorBeamDistanceToActivate;
                    Rigidbody2D selectedBody = null;
                    foreach (Rigidbody2D body in Rigidbodies)
                    {
                        if (body != Rigidbody2D && body.GetComponent<BoxCollider2D>() != null)
                        {
                            float dist = Vector2.Distance(body.transform.position, transform.position);
                            if (dist < body.GetComponent<BoxCollider2D>().size.x + TractorBeamDistanceToActivate && dist < lastSmallestDistance)
                            {
                                lastSmallestDistance = dist;
                                selectedBody = body;
                            }
                        }
                    }

                    if (selectedBody != null)
                    {
                        TractoringObject = selectedBody;
                        TractoringObjectDrag = selectedBody.drag;
                        selectedBody.drag = TractoredObjectDrag;
                    }
                }
                else
                {
                    TractoringObject.drag = TractoringObjectDrag;
                    TractoringObject = null;
                }
            }
        }

        protected virtual void FixedUpdate()
        {
            Vector2 tractorPoint = transform.position - transform.up / 2f;

            if (TractoringObject != null && Vector2.Distance(TractoringObject.transform.position, tractorPoint) > TractoringObject.GetComponent<SpriteRenderer>().size.x + TractorBeamDistanceBehind)
            {
                TractoringObject.AddForce((transform.position - TractoringObject.transform.position).normalized * TractorBeamStrength * Time.smoothDeltaTime);
            }
        }

        protected virtual void OnDrawGizmos()
        {
            if (TractoringObject != null)
            {
                Debug.DrawLine(transform.position, TractoringObject.transform.position, Color.blue, 0.02f);
            }
        }
    }
}