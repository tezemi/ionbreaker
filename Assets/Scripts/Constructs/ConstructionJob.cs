﻿using UnityEngine;

namespace Ionbreaker.Constructs
{
    [CreateAssetMenu(fileName = "New Construction Job", menuName = "Data/Construction Job")]
    public class ConstructionJob : ScriptableObject
    {
        public int Cost = 200;
        public float Time = 5f;
        public GameObject Prefab;
    }
}

