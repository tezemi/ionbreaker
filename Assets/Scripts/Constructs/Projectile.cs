﻿using Ionbreaker.Voxels;
using UnityEngine;

namespace Ionbreaker.Constructs
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(SpriteRenderer))]
    public class Projectile : MonoBehaviour
    {
        public int Damage = 10;
        public float Speed = 100f;
        protected Rigidbody2D Rigidbody2D { get; set; }

        protected virtual void Awake()
        {
            Rigidbody2D = GetComponent<Rigidbody2D>();
        }

        protected virtual void Update()
        {
            transform.up = Rigidbody2D.velocity;
        }

        protected virtual void OnCollisionEnter2D(Collision2D other)
        {
            if (other.gameObject.GetComponent<Construct>() != null)
            {
                other.gameObject.GetComponent<Construct>().Hurt(10);
            }
            else if (other.gameObject.GetComponent<Voxel>() != null)
            {
                other.gameObject.GetComponent<Voxel>().Mine(1f);
            }

            Destroy(gameObject);
        }

        public virtual void Fire()
        {
            Rigidbody2D.AddForce(transform.up * Speed);
        }
    }
}
