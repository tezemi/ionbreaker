﻿using UnityEngine;

namespace Ionbreaker.Constructs
{
    /// <summary>
    /// Base class for ship constructs that can be flown around by players. 
    /// Handles basic movement.
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class Starbase : Construct
    {
        public ConstructionJob ShipConstructionJob;

        protected virtual void Update()
        {
            if (!Focused) return;

            if (Input.GetKeyDown(KeyCode.Alpha1) && Owner.SpaceDust > ShipConstructionJob.Cost)
            {
                BeginConstruction(ShipConstructionJob);
            }
        }
    }
}